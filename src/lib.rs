//! # Overview
//! 
//! A crate for buffered readers with a sliding window to access the underlying data. Aimed at parsers and other
//! applications which require buffering, offering the reduced memory usage of streaming approach.
//! 
//! The crate provides data-structures that buffer an underlying reader while providing access to the buffered data via
//! a movable window, automatically managing the buffer.
mod either_iter;
mod raw;
mod window_reader;
mod string;
mod window;

pub use raw::*;
pub use string::*;
pub use crate::window_reader::*;