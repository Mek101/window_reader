mod linear;
//mod circular;

use std::{
    alloc::{self, Layout},
    io,
};

pub use linear::*;
//pub use circular::*;

pub(crate) const DEFAULT_BUF_CAPACITY: usize = 8192; // 8 KBs.

pub(crate) fn alloc_buffer(size: usize) -> Result<Box<[u8]>, io::Error> {
    if size == 0 {
        return Ok(Box::<[u8]>::default());
    }

    let layout = Layout::array::<u8>(size)
        .map_err(|_| io::Error::new(io::ErrorKind::InvalidInput, "Allocation too big"))?;
    let ptr = unsafe { alloc::alloc_zeroed(layout) };
    if ptr.is_null() {
        return Err(io::Error::new(
            io::ErrorKind::InvalidInput,
            "Allocation failed",
        ));
    }
    let slice_ptr = core::ptr::slice_from_raw_parts_mut(ptr, size);
    Ok(unsafe { Box::from_raw(slice_ptr) })
}

pub(crate) fn min_for(base: usize, min: usize) -> usize {
    let base = if base == 0 {
        min
    } else {
        base
    };

    let mut m = base + min - 1;
    m -= m % min;

    m
}
