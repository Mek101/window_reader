pub(crate) enum EitherIterator<T, L, R>
where
    L: Iterator<Item = T>,
    R: Iterator<Item = T>,
{
    Left { left: L },
    Right { right: R },
}

impl<T, L, R> Iterator for EitherIterator<T, L, R>
where
    L: Iterator<Item = T>,
    R: Iterator<Item = T>,
{
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        match self {
            EitherIterator::Left { left } => left.next(),
            EitherIterator::Right { right } => right.next(),
        }
    }
}
