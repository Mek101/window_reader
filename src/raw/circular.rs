use std::{
    cmp,
    io::{self, IoSliceMut, Read},
};

pub struct CircularRawWindowReader<R: Read> {
    reader: R,
    buf: Box<[u8]>,
    base_size: usize,
    tail: usize,
    window_head: usize,
    buffered_head: usize,
}

impl<R: Read> CircularRawWindowReader<R> {
    fn copy_contiguous(size: usize, a: &[u8], b: &[u8]) -> Box<[u8]> {
        let mut buf = super::alloc_buffer(size);
        buf[..a.len() - 1].copy_from_slice(a);
        buf[a.len()..a.len() + b.len() - 1].copy_from_slice(b);

        buf
    }

    fn unbuffered_io_array_of(
        buf: &mut [u8],
        buffered_head: usize,
        tail: usize,
    ) -> [IoSliceMut; 2] {
        if buffered_head >= tail {
            let (a, b) = buf.split_at_mut(buffered_head);

            [
                IoSliceMut::new(a),
                IoSliceMut::new(&mut b[buffered_head - tail..]),
            ]
        } else {
            [
                IoSliceMut::new(&mut buf[buffered_head..tail]),
                IoSliceMut::new(&mut []),
            ]
        }
    }

    pub fn new(reader: R) -> Self {
        Self::with_base_capacity(reader, super::DEFAULT_BUF_CAPACITY)
    }

    pub fn with_base_capacity(reader: R, capacity: usize) -> Self {
        Self {
            reader,
            buf: super::alloc_buffer(capacity),
            base_size: capacity,
            tail: 0,
            window_head: 0,
            buffered_head: 0,
        }
    }

    pub fn window_size(&self) -> usize {
        if self.window_head >= self.tail {
            self.window_head - self.tail
        } else {
            self.window_head + self.buf.len() - self.tail
        }
    }

    pub fn buffered_size(&self) -> usize {
        if self.buffered_head >= self.tail {
            self.buffered_head - self.tail
        } else {
            self.buffered_head + self.buf.len() - self.tail
        }
    }

    pub fn window(&self) -> (&[u8], &[u8]) {
        if self.tail <= self.window_head {
            (&self.buf[self.tail..self.window_head], &[])
        } else {
            (&self.buf[self.tail..], &self.buf[..self.window_head])
        }
    }

    pub fn advance_window(&mut self, advance: usize) -> Result<usize, io::Error> {
        todo!()
    }

    pub fn advance_tail(&mut self, advance: usize) -> usize {
        let window_size = self.window_size();

        let advance = cmp::min(advance, window_size);
        self.tail = (self.tail + advance) % self.buf.len();
        assert!(self.window_size() == window_size - advance);

        let buffered_size = self.buffered_size();

        advance
    }

    pub fn advance_head(&mut self, advance: usize) -> Result<usize, io::Error> {
        let window_size = self.window_size();
        let buffered_size = self.buffered_size();

        assert!(window_size <= buffered_size);

        let new_window_size = window_size + advance;

        // Advance within the buffered size.
        if new_window_size > buffered_size {
            // Advance also requires extending the buffer.
            if new_window_size > self.buf.len() {
                let new_len = super::min_for(self.base_size, new_window_size);
                assert!(new_len % self.base_size == 0);
                assert!(new_len >= new_window_size);

                // Move the buffer's content's while making it contiguous.
                let (a, b) = self.window();
                let new_buf = Self::copy_contiguous(new_len, a, b);

                self.buf = new_buf;
                self.tail = 0;
                self.window_head = window_size;
                self.buffered_head = buffered_size;
            }

            let reader = &mut self.reader;
            let mut io_arr =
                Self::unbuffered_io_array_of(&mut self.buf, self.buffered_head, self.tail);
            let written = reader.read_vectored(&mut io_arr)?;

            self.buffered_head = (self.buffered_head + written) % self.buf.len();
        }

        let advance = cmp::min(advance, self.buffered_size() - self.window_size());
        self.window_head = (self.window_head + advance) % self.buf.len();

        assert!(self.window_size() == window_size + advance);

        Ok(advance)
    }
}

