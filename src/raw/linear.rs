use std::{
    cmp,
    io::{self, Read},
};

use crate::window_reader::WindowReader;

pub struct LinearRawWindowReader<R: Read> {
    reader: R,
    buf: Box<[u8]>,
    base_size: usize,
    tail: usize,
    head: usize,
    buffered: usize,
}

impl<R: Read> LinearRawWindowReader<R> {
    /// Create a new `LinearRawWindowReader`.
    pub fn new(reader: R) -> Result<Self, io::Error> {
        Self::with_base_capacity(reader, super::DEFAULT_BUF_CAPACITY)
    }

    /// Create a new `LinearRawWindowReader` with a base capacity. The buffer may grow to be a multiple of the given base capacity.
    pub fn with_base_capacity(reader: R, capacity: usize) -> Result<Self, io::Error> {
        Ok(Self {
            reader,
            buf: super::alloc_buffer(capacity)?,
            base_size: capacity,
            tail: 0,
            head: 0,
            buffered: 0,
        })
    }

    /// Size of the internal buffer.
    #[inline]
    pub fn buffered_size(&self) -> usize {
        self.buffered
    }
}

impl<R: Read> WindowReader<io::Error> for LinearRawWindowReader<R> {
    type Window<'w> = &'w [u8] where Self: 'w;

    /// Access the window.
    fn window(&self) -> &[u8] {
        &self.buf[self.tail..self.head]
    }

    /// Size of the window.
    #[inline]
    fn window_size(&self) -> usize {
        self.head - self.tail
    }

    /// Advances the window's tail by up to `advance` slots, compacting it.
    fn advance_tail(&mut self, advance: usize) -> usize {
        let advance = cmp::min(advance, self.window_size());
        self.tail += advance;

        advance
    }

    /// Advances the window's head by up to `advance` slots, expanding it.
    fn advance_head(&mut self, advance: usize) -> Result<usize, io::Error> {
        if advance <= self.buffered - self.head {
            self.head += advance;
            Ok(advance)
        } else {
            let window_size = self.window_size();

            // Advance also requires extending the buffer, or the window uses less than 1/3ish of the buffer.
            if advance + window_size > self.buf.len() || advance + window_size <= self.buf.len() / 2 + self.buf.len() / 4 {
                let new_len = super::min_for(self.base_size, window_size + advance);
                let mut new_buf = super::alloc_buffer(new_len)?;

                new_buf[..self.buffered - self.tail]
                    .copy_from_slice(&self.buf[self.tail..self.buffered]);
                self.buf = new_buf;

                self.buffered -= self.tail;
                self.head = window_size;
                self.tail = 0;
            }
            // Shifting back the window and the buffered data is sufficient.
            else {
                self.buf.copy_within(self.tail..self.buffered, 0);

                self.buffered -= self.tail;
                self.head = window_size;
                self.tail = 0;
            }

            let read_buf = &mut self.buf[self.buffered..];
            let written = self.reader.read(read_buf)?;
            self.buffered += written;
            assert!(self.buffered <= self.buf.len());

            let advance = cmp::min(advance, self.buffered - self.window_size());

            self.head += advance;
            assert!(self.head <= self.buffered);
            assert!(self.tail <= self.head);

            Ok(advance)
        }
    }

    /// Advances the window's head by up to `advance` slots, expanding it without calling the underlying reader.
    fn advance_head_quick(&mut self, advance: usize) -> usize {
        let advance = cmp::min(advance, self.buffered - self.head);

        self.head += advance;
        advance
    }

    /// Retracts the window's head by up to `backtrack` slots, compacting it.
    fn backtrack_head(&mut self, backtrack: usize) -> usize {
        let backtrack = cmp::min(backtrack, self.window_size());

        self.head -= backtrack;
        backtrack
    }

    fn reset_window_at(&mut self, offset: usize) -> Result<usize, io::Error> {
        // The offset is within the window.
        if self.tail + offset <= self.buffered {
            self.tail += offset;
            self.head = self.tail;
            Ok(offset)
        }
        // The offset would require to walk multiple buffers. Skip them.
        else if self.tail + offset / self.base_size > 1 {
            // Early alloc, early fail without data loss.
            let new_buf = super::alloc_buffer(self.base_size)?;

            let to_skip = (self.tail + offset - self.base_size) as u64;
            assert!(to_skip as usize > self.base_size);
            let advanced =
                io::copy(&mut self.reader.by_ref().take(to_skip), &mut io::sink())? as usize;

            self.buf = new_buf;
            self.tail = 0;
            self.buffered = 0;
            self.head = 0;

            Ok(advanced)
        }
        // The offset is just outside the current buffered bytes.
        else {
            // Early alloc, early fail without data loss.
            let mut new_buf = super::alloc_buffer(self.base_size)?;

            let written = self.reader.read(&mut new_buf)?;
            self.buf = new_buf;
            self.buffered = written;

            let advance = cmp::min(written, offset - self.base_size);
            self.tail = advance;
            self.head = advance;

            assert!(self.window_size() == 0);
            Ok(advance)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn buf() -> Vec<u8> {
        (0..128).into_iter().collect::<Vec<_>>()
    }

    #[test]
    fn advance_head_tail() {
        let b = buf();
        let b = b.as_slice();

        let mut wr = LinearRawWindowReader::with_base_capacity(b, 8).unwrap();
        assert_eq!(wr.window_size(), 0);
        assert_eq!(wr.window().len(), 0);

        wr.advance_head(4).unwrap();
        assert_eq!(wr.window_size(), 4);
        assert_eq!(wr.window().len(), 4);
        assert_eq!(wr.window(), &[0, 1, 2, 3]);

        wr.advance_tail(1);
        assert_eq!(wr.window_size(), 3);
        assert_eq!(wr.window().len(), 3);
        assert_eq!(wr.window(), &[1, 2, 3]);

        wr.advance_head(4).unwrap();
        assert_eq!(wr.window_size(), 7);
        assert_eq!(wr.window().len(), 7);
        assert_eq!(wr.window(), &[1, 2, 3, 4, 5, 6, 7]);
    }

    #[test]
    fn realloc() {
        let b = buf();
        let b = b.as_slice();

        let mut wr = LinearRawWindowReader::with_base_capacity(b, 8).unwrap();

        wr.advance_head(18).unwrap();
        assert_eq!(wr.window_size(), 18);
        assert_eq!(wr.window().len(), 18);
        assert_eq!(
            wr.window(),
            &[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]
        );
    }

    #[test]
    fn backtrack() {
        let b = buf();
        let b = b.as_slice();

        let mut wr = LinearRawWindowReader::with_base_capacity(b, 8).unwrap();
        wr.advance_head(4).unwrap();

        wr.backtrack_head(3);
        assert_eq!(wr.window_size(), 1);
        assert_eq!(wr.window().len(), 1);
        assert_eq!(wr.window(), &[0]);
    }

    #[test]
    fn inverted() {
        let b = buf();
        let b = &b.as_slice()[..16];

        let mut wr = LinearRawWindowReader::with_base_capacity(b, 10).unwrap();
        assert_eq!(wr.window_size(), 0);
        assert_eq!(wr.window().len(), 0);

        wr.advance_head(4).unwrap();
        assert_eq!(wr.window_size(), 4);
        assert_eq!(wr.window().len(), 4);
        assert_eq!(wr.window(), &[0, 1, 2, 3]);

        wr.advance_tail(1);
        assert_eq!(wr.window_size(), 3);
        assert_eq!(wr.window().len(), 3);
        assert_eq!(wr.window(), &[1, 2, 3]);

        wr.advance_head(80).unwrap();
        assert_eq!(wr.window_size(), 15);
        assert_eq!(wr.window().len(), 15);
        assert_eq!(
            wr.window(),
            &[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
        );
    }

    #[test]
    fn zero_capacity() {
        let b = buf();
        let b = &b.as_slice()[..16];

        let mut wr = LinearRawWindowReader::with_base_capacity(b, 0).unwrap();
        assert_eq!(wr.window_size(), 0);
        assert_eq!(wr.window().len(), 0);

        wr.advance_head(4).unwrap();
        assert_eq!(wr.window_size(), 4);
        assert_eq!(wr.window().len(), 4);
        assert_eq!(wr.window(), &[0, 1, 2, 3]);

        wr.advance_tail(1);
        assert_eq!(wr.window_size(), 3);
        assert_eq!(wr.window().len(), 3);
        assert_eq!(wr.window(), &[1, 2, 3]);
    }
}
