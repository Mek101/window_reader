use std::{
    io::{self, Read},
    iter, str,
};

use crate::{
    either_iter::EitherIterator, window::Window2Ext, InvalidSequence, LinearRawWindowReader,
    ParseError, StringSlicerIterator, WindowReader,
};

pub struct LinearStringWindowReader<R: Read> {
    inner: LinearRawWindowReader<R>,
    chars_count: usize,
}

impl<R: Read> LinearStringWindowReader<R> {
    fn lengths_in(buf: &[u8]) -> impl Iterator<Item = (usize, bool)> + '_ {
        StringSlicerIterator::slice_buf(buf)
            .map_while(|r| match r {
                Ok(string) => {
                    // First item is the character length in bytes, second is wherever it should be counted as a character.
                    let chars_len = string
                        .char_indices()
                        .map(|(idx, _)| idx)
                        .chain(iter::once(string.len()))
                        .window2()
                        .map(|(prev, curr)| (curr - prev, true));

                    Some(EitherIterator::Left { left: chars_len })
                }
                Err(err) => match err {
                    ParseError::Incomplete => None,
                    ParseError::InvalidSequence { seq } => Some(EitherIterator::Right {
                        right: iter::once((seq.len(), false)),
                    }),
                },
            })
            .flatten()
    }

    /// Returns the number of bytes and the number of characters mapping to those bytes.
    fn count_bytes_up_to_chars(mut buf: &[u8], chars: usize) -> (usize, usize) {
        let mut counted_bytes = 0;
        let mut chars_left = chars;

        for (byte_size, is_char) in Self::lengths_in(buf) {
            counted_bytes += byte_size;
            if is_char {
                chars_left -= 1;
                if chars_left == 0 {
                    break;
                }
            }
            buf = &buf[byte_size..];
        }

        let walked_chars = chars - chars_left;

        (counted_bytes, walked_chars)
    }

    /// Does not backtrack. Returns the number of chars and superfluous bytes it advanced by.
    fn advance_head_quick_inner(&mut self, advance: usize) -> (usize, usize) {
        // UTF-8 characters are max 4 bytes long.
        let worst_case = advance * 4;

        let inner_advanced_bytes = self.inner.advance_head_quick(worst_case);
        if inner_advanced_bytes > 0 {
            let window = &self.inner.window()[self.inner.window_size() - inner_advanced_bytes..];
            let (effective_bytes, chars_advanced) = Self::count_bytes_up_to_chars(window, advance);

            let superfluous_bytes = inner_advanced_bytes - effective_bytes;
            (chars_advanced, superfluous_bytes)
        } else {
            (0, 0)
        }
    }

    /// Create a new `LinearStringWindowReader`.
    pub fn new(reader: R) -> Result<Self, io::Error> {
        Ok(Self {
            inner: LinearRawWindowReader::new(reader)?,
            chars_count: 0,
        })
    }

    /// Create a new `LinearStringWindowReader` with a base capacity. The buffer may grow to be a multiple of the given base capacity.
    pub fn with_base_capacity(reader: R, capacity: usize) -> Result<Self, io::Error> {
        Ok(Self {
            inner: LinearRawWindowReader::with_base_capacity(reader, capacity)?,
            chars_count: 0,
        })
    }

    /// Size of the internal byte buffer.
    pub fn buffered_size(&self) -> usize {
        self.inner.buffered_size()
    }
}

impl<R: Read> WindowReader<io::Error> for LinearStringWindowReader<R> {
    type Window<'w> = LinearStringWindowIterator<'w> where Self: 'w;

    /// Access the window.
    fn window(&self) -> Self::Window<'_> {
        LinearStringWindowIterator {
            inner: StringSlicerIterator::slice_buf(self.inner.window()),
        }
    }

    /// The size of the window, in chars.
    #[inline]
    fn window_size(&self) -> usize {
        self.chars_count
    }

    /// Advances the window's tail by up to `advance` slots, compacting it.
    fn advance_tail(&mut self, advance: usize) -> usize {
        // Happy path.
        if advance >= self.chars_count {
            self.inner.next_window();

            let chars_count = self.chars_count;
            // Keep track of the characters in the window.
            self.chars_count = 0;
            chars_count
        }
        // Sad path :-C
        else {
            let (bytes, chars) = Self::count_bytes_up_to_chars(self.inner.window(), advance);

            self.inner.advance_tail(bytes);

            // Keep track of the characters in the window.
            self.chars_count -= chars;
            chars
        }
    }

    /// Advances the window's head by up to `advance` slots, expanding it.
    fn advance_head(&mut self, advance: usize) -> Result<usize, io::Error> {
        // Since `advance_head_quick_inner` advances by the worst case scenario, which might cause extra unneeded IO and
        // reallocations, advance only within the already buffered bytes first.
        let (chars_advanced, superfluous_bytes) = self.advance_head_quick_inner(advance);

        // Characters left to advance.
        let to_advance = advance - chars_advanced;
        // Superfluous bytes that should be backtracked by.
        let mut to_backtrack = superfluous_bytes;
        // Characters this function advanced by.
        let mut advanced = chars_advanced;

        if to_advance > 0 {
            // UTF-8 characters are max 4 bytes long. Subtract the number of previously superfluous bytes since they're
            // already loaded in.
            let worst_case = advance * 4 - superfluous_bytes;

            let inner_advanced_bytes = self.inner.advance_head(worst_case)?;
            if inner_advanced_bytes > 0 {
                // Count the number of chars (and bytes) we just advanced by.
                let window =
                    &self.inner.window()[self.inner.window_size() - inner_advanced_bytes..];
                let (effective_bytes, chars_advanced) =
                    Self::count_bytes_up_to_chars(window, advance);

                to_backtrack = inner_advanced_bytes - effective_bytes;
                advanced += chars_advanced;
            }
        }

        // Backtrack the inner buffer by the number of superfluous bytes it was advanced by.
        assert_eq!(self.inner.backtrack_head(to_backtrack), to_backtrack);

        // Keep track of the characters in the window.
        self.chars_count += advanced;
        Ok(advanced)
    }

    /// Advances the window's head by up to `advance` slots, expanding it without calling the underlying reader.
    fn advance_head_quick(&mut self, advance: usize) -> usize {
        let (chars_advanced, superfluous_bytes) = self.advance_head_quick_inner(advance);

        // Backtrack the inner buffer by the number of superfluous bytes it was advanced by.
        assert_eq!(
            self.inner.backtrack_head(superfluous_bytes),
            superfluous_bytes
        );

        // Keep track of the characters in the window.
        self.chars_count += chars_advanced;
        chars_advanced
    }

    /// Retracts the window's head by up to `backtrack` slots, compacting it.
    fn backtrack_head(&mut self, backtrack: usize) -> usize {
        // Happy path.
        if backtrack >= self.chars_count {
            self.inner.backtrack_head(usize::MAX);

            let chars_count = self.chars_count;
            // Keep track of the characters in the window.
            self.chars_count = 0;
            chars_count
        }
        // Sad path :-C
        else {
            // Can't count the characters backwards.
            let chars_to_keep = self.chars_count - backtrack;
            // Get the size in bytes of the window that should be.
            let (bytes, chars_counted) =
                Self::count_bytes_up_to_chars(self.inner.window(), chars_to_keep);
            assert_eq!(chars_to_keep, chars_counted);

            let bytes_to_backtrack = self.inner.window_size() - bytes;

            assert_eq!(
                self.inner.backtrack_head(bytes_to_backtrack),
                bytes_to_backtrack
            );

            self.chars_count -= backtrack;
            backtrack
        }
    }

    /// Closes the current window by advancing it.
    fn next_window(&mut self) {
        self.chars_count = 0;
        self.inner.next_window()
    }

    fn reset_window_at(&mut self, offset: usize) -> Result<usize, io::Error> {
        if offset <= self.chars_count {
            self.advance_tail(offset);
            self.inner.backtrack_head(usize::MAX);

            // Keep track of the characters in the window.
            self.chars_count = 0;
            Ok(offset)
        } else {
            let advanced = self.advance_head(offset)?;
            self.inner.advance_tail(usize::MAX);

            // Keep track of the characters in the window.
            self.chars_count = 0;
            Ok(advanced)
        }
    }
}

pub struct LinearStringWindowIterator<'r> {
    inner: StringSlicerIterator<'r>,
}

impl<'r> Iterator for LinearStringWindowIterator<'r> {
    type Item = Result<&'r str, InvalidSequence<'r>>;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            match self.inner.next()? {
                Ok(string) => return Some(Ok(string)),
                Err(err) => match err {
                    ParseError::Incomplete => {} // Continue looping.
                    ParseError::InvalidSequence { seq } => {
                        return Some(Err(InvalidSequence { seq }))
                    }
                },
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    static BUF: &'static str =
        "&J!P么Löwe 老虎 Léopard赫l¿ɨߒ֠ӌΆƮ̙̤🮂🍳🚋🔷🠣🧾🗮🮥🌮🠈🟅🧉😥ⓄⰔ∘Ⰿ开伊艾X#SqYh=";

    #[test]
    fn advance_head_tail() {
        let mut wr = LinearStringWindowReader::with_base_capacity(BUF.as_bytes(), 8).unwrap();
        assert_eq!(wr.window_size(), 0);
        assert_eq!(
            wr.window()
                .filter_map(|r| r.ok())
                .collect::<String>()
                .as_str(),
            ""
        );

        wr.advance_head(21).unwrap();
        assert_eq!(wr.window_size(), 21);
        assert_eq!(
            wr.window()
                .filter_map(|r| r.ok())
                .collect::<String>()
                .as_str(),
            BUF.chars().take(21).collect::<String>().as_str()
        );

        wr.advance_tail(5);
        assert_eq!(wr.window_size(), 16);
        assert_eq!(
            wr.window()
                .filter_map(|r| r.ok())
                .collect::<String>()
                .as_str(),
            BUF.chars().skip(5).take(16).collect::<String>().as_str()
        );

        wr.advance_head(4).unwrap();
        assert_eq!(wr.window_size(), 20);
        assert_eq!(
            wr.window()
                .filter_map(|r| r.ok())
                .collect::<String>()
                .as_str(),
            BUF.chars().skip(5).take(20).collect::<String>().as_str()
        );
    }

    #[test]
    fn realloc() {
        let mut wr = LinearStringWindowReader::with_base_capacity(BUF.as_bytes(), 8).unwrap();

        wr.advance_head(18).unwrap();
        assert_eq!(wr.window_size(), 18);
        assert_eq!(
            wr.window()
                .filter_map(|r| r.ok())
                .collect::<String>()
                .as_str(),
            BUF.chars().take(18).collect::<String>().as_str()
        );
    }

    #[test]
    fn backtrack() {
        let mut wr = LinearStringWindowReader::with_base_capacity(BUF.as_bytes(), 8).unwrap();
        wr.advance_head(4).unwrap();

        wr.backtrack_head(3);
        assert_eq!(wr.window_size(), 1);
        assert_eq!(
            wr.window()
                .filter_map(|r| r.ok())
                .collect::<String>()
                .as_str(),
            BUF.chars().take(1).collect::<String>().as_str()
        );
    }
}
