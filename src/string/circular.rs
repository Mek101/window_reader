use std::{
    borrow::Cow,
    cmp,
    io::{self, Read},
    str,
};

use crate::{
    buf_slicer,
    string_util::{self, ParseError, StringSlicerIterator},
    CircularRawWindowReader, LinearRawWindowReader,
};

pub struct CircularStringWindowReader<R: Read> {
    inner: CircularRawWindowReader<R>,
}

impl<R: Read> CircularStringWindowReader<R> {
    pub fn new(reader: R) -> Self {
        Self {
            inner: CircularRawWindowReader::new(reader),
        }
    }

    pub fn with_base_capacity(reader: R, capacity: usize) -> Self {
        Self {
            inner: CircularRawWindowReader::with_base_capacity(reader, capacity),
        }
    }

    pub fn window(&self) -> CircularStringWindowIterator {
        let (a, b) = self.inner.window();
        CircularStringWindowIterator { a, b, pos: 0 }
    }

    /*fn advance_head(&mut self, advance: usize) -> Result<usize, io::Error> {
        let mut chars_advanced = 0;
        let start_window_size = self.inner.window_size();

        loop {
            let mut bytes_advanced_current_char = 0;
            loop {
                let byte_advanced = self.inner.advance_head(1)?;
                if byte_advanced == 0 {
                    return Ok(chars_advanced);
                }

                let (a, b) = self.inner.window();
                let head_slice = pick_head(start_window_size + bytes_advanced_current_char, a, b);
                assert!(!head_slice.is_empty());
            }
        }
    }*/
}

pub struct CircularStringWindowIterator<'r> {
    a: &'r [u8],
    b: &'r [u8],
    pos: usize,
}

impl<'r> CircularStringWindowIterator<'r> {
    fn string_within_or_err<'b>(
        buf: &'b [u8],
        index: usize,
    ) -> Option<Result<Cow<'b, str>, ParseError>> {
        if index > buf.len() {
            None
        } else {
            let buf = &buf[index..];
            Some(string_util::next_string_from(&buf).map(|string| Cow::Borrowed(string)))
        }
    }

    fn paste_buffers(a: &[u8], b: &[u8]) -> [u8; 4] {
        assert!(a.len() < 4);
        let mut dst = [0; 4];
        let b_end = cmp::min(b.len(), 4 - a.len());

        dst[..a.len()].copy_from_slice(a);
        dst[a.len() - 1..].copy_from_slice(&b[..b_end]);

        dst
    }
}

impl<'r> Iterator for CircularStringWindowIterator<'r> {
    type Item = Cow<'r, str>;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            if let Some(r) = Self::string_within_or_err(self.a, self.pos) {
                match r {
                    Ok(string) => {
                        self.pos += string.len();
                        return Some(string);
                    }
                    Err(err) => {
                        match err {
                            ParseError::Incomplete => {
                                if self.b.len() != 0 {
                                    // The char is incomplete and we have a successive buffer, so try paste the two.
                                    let buffer = Self::paste_buffers(&self.a[self.pos..], self.b);

                                    match string_util::next_string_from(&buffer) {
                                        Ok(string) => {
                                            self.pos += string.len();
                                            return Some(Cow::Owned(string.to_owned()));
                                        }
                                        Err(err) => {
                                            match err {
                                                ParseError::Incomplete => {
                                                    // No next buffer to fetch from. The iterator ends here :-C
                                                    return None;
                                                }
                                                ParseError::InvalidSequence { len } => {
                                                    self.pos += len
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    // No next buffer to fetch from. The iterator ends here :-C
                                    return None;
                                }
                            }
                            ParseError::InvalidSequence { len } => self.pos += len,
                        }
                    }
                }
            } else if let Some(r) = Self::string_within_or_err(self.b, self.pos - self.a.len()) {
                match r {
                    Ok(string) => {
                        self.pos += string.len();
                        return Some(string);
                    }
                    Err(err) => {
                        match err {
                            ParseError::Incomplete => {
                                // No next buffer to fetch from. The iterator ends here :-C
                                return None;
                            }
                            ParseError::InvalidSequence { len } => self.pos += len,
                        }
                    }
                }
            } else {
                return None;
            }
        }
    }
}