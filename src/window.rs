use std::iter::Peekable;

pub struct Window2<T, I>
where
    I: Iterator<Item = T>,
    T: ToOwned,
{
    inner: Peekable<I>,
}

impl<T, I> Iterator for Window2<T, I>
where
    I: Iterator<Item = T>,
    T: Clone,
{
    type Item = (T, T);

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(a) = self.inner.next() {
            if let Some(b) = self.inner.peek().map(|item| item.clone()) {
                return Some((a, b));
            }
        }

        None
    }
}

pub trait Window2Ext: Iterator
where
    Self: Sized,
    Self::Item: Clone,
{
    fn window2(self) -> Window2<Self::Item, Self> {
        Window2 {
            inner: self.peekable(),
        }
    }
}

impl<I> Window2Ext for I
where
    I: Iterator,
    I::Item: Clone,
{
}
