//! A generic interface for a window reader
pub trait WindowReader<E> {
    type Window<'w>
    where
        Self: 'w;

    /// Access the window.
    fn window(&self) -> Self::Window<'_>;
    /// Size of the window.
    fn window_size(&self) -> usize;

    /// Advances the window's tail by up to `advance` slots, compacting it.
    fn advance_tail(&mut self, advance: usize) -> usize;
    /// Advances the window's head by up to `advance` slots, expanding it.
    fn advance_head(&mut self, advance: usize) -> Result<usize, E>;
    /// Advances the window's head by up to `advance` slots, expanding it without calling the underlying reader.
    fn advance_head_quick(&mut self, _advance: usize) -> usize {
        0
    }
    /// Retracts the window's head by up to `backtrack` slots, compacting it.
    fn backtrack_head(&mut self, backtrack: usize) -> usize;

    /// Slide the window forward by up to `advance` slots.
    /// Returns by how many slots the window was moved.
    fn slide_window(&mut self, advance: usize) -> Result<usize, E> {
        let window_size = self.window_size();

        let advanced = self.advance_tail(advance);
        let _ = self.advance_head(self.window_size() - window_size);

        Ok(advanced)
    }

    /// Slide the window forward by up to `advance` slots, without calling the underlying reader.
    /// Returns by how many slots the window was moved.
    fn slide_window_quick(&mut self, advance: usize) -> usize {
        let window_size = self.window_size();

        let advanced = self.advance_tail(advance);
        let _ = self.advance_head_quick(self.window_size() - window_size);

        advanced
    }

    /// Slide the window forward by up to `advance` slots, without calling the underlying reader.
    /// The head will not be moved if the advanced window size is at least `min_size`.
    /// Return by how many slots the window was moved.
    fn slide_window_resizing(&mut self, advance: usize, min_size: usize) -> Result<usize, E> {
        let advance = self.advance_tail(advance);
        let window_size = self.window_size();
        if window_size < min_size {
            let _ = self.advance_head(min_size - window_size)?;
        }

        Ok(advance)
    }

    /// Slide the window forward by up to `advance` slots, without calling the underlying reader.
    /// The head will not be moved if the advanced window size is at least `min_size`.
    /// Return by how many slots the window was moved.
    fn slide_window_resizing_quick(&mut self, advance: usize, min_size: usize) -> usize {
        let advance = self.advance_tail(advance);
        let window_size = self.window_size();
        if window_size < min_size {
            let _ = self.advance_head_quick(min_size - window_size);
        }

        advance
    }

    /// Closes the current window by advancing it.
    fn next_window(&mut self) {
        let _ = self.advance_tail(usize::MAX);
    }

    /// Tries to ensure the window size is at least `size` slots.
    /// Returns the new size of the window. May be less than `size` if the reader was drained.
    fn window_ensure_size(&mut self, size: usize) -> Result<usize, E> {
        if self.window_size() < size {
            let _ = self.advance_head(size - self.window_size())?;
        }

        Ok(self.window_size())
    }

    /// Tries to ensure the window size is at least `size` slots, without calling the underlying reader.
    /// Returns the new size of the window. May be less than `size` if a call to the reader would have been necessary.
    fn window_ensure_size_quick(&mut self, size: usize) -> usize {
        if self.window_size() < size {
            let _ = self.advance_head_quick(size - self.window_size());
        }

        self.window_size()
    }

    fn reset_window_at(&mut self, offset: usize) -> Result<usize, E> {
        if offset < self.window_size() {
            self.advance_tail(offset);
            self.backtrack_head(usize::MAX);
            Ok(offset)
        } else {
            let advanced = self.advance_head(offset - self.window_size())?;
            self.next_window();
            Ok(advanced)
        }
    }
}
