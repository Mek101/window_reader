mod linear;
//mod circular;

use std::{str, error::Error, fmt::Display};

pub use linear::*;
//pub use circular::*;

#[derive(Debug)]
pub struct InvalidSequence<'r> {
    seq: &'r [u8],
}

impl InvalidSequence<'_> {
    pub fn as_bytes(&self) -> &[u8] {
        self.seq
    }
}

impl Error for InvalidSequence<'_> {}

impl Display for InvalidSequence<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.seq)
    }
}

pub(crate) enum ParseError<'b> {
    Incomplete,
    InvalidSequence { seq: &'b [u8] },
}

pub(crate) fn next_string_from(buf: &[u8]) -> Result<&str, ParseError> {
    match str::from_utf8(buf) {
        Ok(string) => Ok(string),
        Err(err) => {
            if err.valid_up_to() != 0 {
                let string = unsafe { str::from_utf8_unchecked(&buf[..err.valid_up_to()]) };
                Ok(string)
            } else {
                match err.error_len() {
                    Some(seq_len) => Err(ParseError::InvalidSequence {
                        seq: &buf[..seq_len],
                    }),
                    None => Err(ParseError::Incomplete),
                }
            }
        }
    }
}

pub(crate) struct StringSlicerIterator<'b> {
    buf: &'b [u8],
}

impl<'b> StringSlicerIterator<'b> {
    pub fn slice_buf(buf: &'b [u8]) -> Self {
        Self { buf }
    }
}

impl<'b> Iterator for StringSlicerIterator<'b> {
    type Item = Result<&'b str, ParseError<'b>>;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            match next_string_from(self.buf) {
                Ok(string) => {
                    if string.len() == 0 {
                        return None;
                    } else {
                        self.buf = &self.buf[string.len()..];
                        return Some(Ok(string));
                    }
                }
                Err(err) => match err {
                    ParseError::Incomplete => return None,
                    ParseError::InvalidSequence { seq } => {
                        self.buf = &self.buf[seq.len()..];
                        return Some(Err(err));
                    }
                },
            }
        }
    }
}
